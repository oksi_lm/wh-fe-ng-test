/**
 * Update the following components to meet the requirements : 
 * * Bind [field] of [textfield] component to its text input
 * * Pass value of [field] from [textfield] component to [title] property of component [ng-app]
 */
import {Component, EventEmitter, NgModule, Output} from '@angular/core';
import { RouterModule } from "@angular/router";
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';

@Component({
    selector : 'textfield',
    template : '<input type="text" value="" [(ngModel)]="field" /><button (click)="setTitle()">Enter</button>'
})
export class TextField {
    @Output() enterTitle = new EventEmitter();
    field = "";
    setTitle() {this.enterTitle.emit(this.field);}
}

@Component({
    selector : 'child-component',
    template : `<h2>Title:<h2><br/><textfield (enterTitle)="getTitle($event)"></textfield>`
})
export class ChildComponent {
    field: string;
    @Output() passTitle = new EventEmitter();

    getTitle($event) {
        this.field = $event;
        this.passTitle.emit(this.field);
    }
}

@Component({
    selector : 'ng-app',
    template : `<div>
                    <child-component (passTitle)="displayTitle($event)"></child-component> <br/>
                    Title is {{title}}
                </div>`
})
export class Test02Component {

    title: any;

    displayTitle($event) {
        this.title = $event;
    }
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: Test02Component
            }
        ]),
        FormsModule
    ],
    declarations : [Test02Component, ChildComponent, TextField]
})
export class Test02Module {};
