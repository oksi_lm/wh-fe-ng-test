/**
 * Update the following components to meet the requirements :
 *
 * * Bind [email] property to input[name="email"]
 * * Bind [password] property to input[name="password"]
 *
 * Without using angular forms, validate both fields so that :
 * * email is in correct format ( ex: ends with @a.com)
 * * password contains at least one special character, one upper case character, one lower case character, one number and a minium of 8 characters in length
 * * The fields should be validated when trying to submit the form
 * * Prevent the form from doing an actual form submit and instead, after validation pass, turn on the [logged_in] flag
 *
 * You can add error messages below each field that shows if the field is not valid
 */
import {Component, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

@Component({
    selector: 'ng-app',
    template: `
        <form (ngSubmit)="submit($event)">
            <h2>Login</h2>
            <br/>
            <input type="email" [(ngModel)]="email" value="" name="email"/>
            <p *ngIf="emailInvalid">Email is incorrect</p>
            <br/>
            <input type="password" [(ngModel)]="password" value="" name="password"/>
            <p *ngIf="passwordInvalid">Password is incorrect</p>

            <button type="submit">Submit</button>
            <br/><br/>
            <div *ngIf="logged_in">Logged In!</div>
        </form>`
})
export class Test03Component {

    email: string = '';
    password: string = '';
    emailInvalid: boolean = false;
    passwordInvalid: boolean = false;
    logged_in = false;

    submit(event) {
        event.preventDefault();
        if (this.validateEmail() && this.validatePassword()) {
            this.logged_in = true;
            this.emailInvalid = false;
            this.passwordInvalid = false;
        }

        if (!this.validateEmail()) {
            this.emailInvalid = true;
        }

        if (!this.validatePassword()) {
            this.passwordInvalid = true;
        }
    }

    validateEmail() {
        const regex = /\S+@\S+\.\S+/;
        return regex.test(this.email);
    }

    validatePassword() {
        const passReg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,10}/;
        return passReg.test(this.password);
    }
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: Test03Component
            }
        ]),
        FormsModule
    ],
    declarations: [Test03Component]
})
export class Test03Module {
};
