/**
 * In the following component, update the code so that when the value of the [loan-amount] is changed:
 * * If it's blank or 0, the values of [monthly_payment] and [late_payment] becomes "N/A",
 * * If it has a value, the value of [monthly_payment] becomes 2% of [loan-ammount] and the value of [late_payment] becomes 5% of [monthly_payment].
 * * Both [monthly_payment] and [late_payment] should print in the template in currency format : $1,234
 */

import { Component, Input, NgModule  } from '@angular/core';
import { RouterModule } from "@angular/router";
import {FormsModule} from '@angular/forms';

@Component({
    selector : 'ng-app',
    template : `<div>
                    <h2>Loan Details</h2>
                    <b>Monthly Payment:</b> {{monthly_payment}} $<br/>
                    <b>Late Payment Fee : {{late_payment}} $</b> <br/> <br/>
                    <label>Loan amount</label> <br/>
                    <input type="text" [(ngModel)]="loan_amount" (change)="calculate()">
                </div>`
})
export class Test01Component {

    loan_amount: any = 1000;
    monthly_payment: any = 200;
    late_payment: number | string = 10;

    calculate() {
        if (this.loan_amount ===  '' || this.loan_amount === 0) {
            this.monthly_payment = 'N/A';
            this.late_payment = 'N/A';
        } else {
            this.monthly_payment = this.loan_amount * 0.02;
            this.late_payment = this.monthly_payment * 0.02;
            this.monthly_payment = this.renderAmount(this.monthly_payment);
            this.late_payment = this.renderAmount(this.late_payment);
        }
    }

    renderAmount(n: number) {
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: Test01Component
            }
        ]),
        FormsModule
    ],
    declarations : [Test01Component]
})
export class Test01Module {}
