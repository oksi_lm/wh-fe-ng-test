/**
 * Add 2 input forms in the following component for first name and last name. Once both forms are filled out by the user, and user has clicked out of the fields, then beside it a username should be automatically generated which should be in the following format: [firstname]_[lastname]_[random integer]
 * First name and last name should be lowercased, and then a random integer between 1 and 9 should be added to the end
 * For example: if the inputs are "John" and "DOE" the generated username could be "john_doe_4" or "john_doe_2"
 */
import { Component, NgModule  } from '@angular/core';
import { RouterModule} from "@angular/router";
import { CommonModule } from '@angular/common';
import {FormControl, ReactiveFormsModule} from '@angular/forms';

@Component({
    selector : 'ng-app',
    template : `
                <h2>Enter your first and last name</h2>
                <div>
                    <input type="text" [formControl]="firstName" (blur)="renderUserName()">
                    <input type="text" [formControl]="secondName" (blur)="renderUserName()">
                    <p>Username is: {{userName}}</p>
                </div>
                `,
    styles : []
})
export class UserNameComponent {
    firstName = new FormControl('');
    secondName = new FormControl('');
    userName: string;

    renderUserName() {
        if (this.firstName.value !== '' && this.secondName.value !== '') {
            const firstName = this.firstName.value.toLowerCase();
            const secondName = this.secondName.value.toLowerCase();
            const rendomNumber = (Math.floor(Math.random() * 10)).toString();

            this.userName = `${firstName}_${secondName}_${rendomNumber}`;
        }
    }

}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: UserNameComponent
            }
        ]),
        ReactiveFormsModule
    ],
    declarations : [UserNameComponent]
})
export class UserNameModule {};
